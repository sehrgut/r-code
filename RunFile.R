source(file = 'GeneralFunctions.R')

cores = 1
if (cores == 0)
{
  stop('cores = 0 \nPlease set the desired number of cores on which the analysis should be run!')
}

# File names are stored in the files variable. In the getFileList function you should point the path to the files

# Load required consts
fs = 125
inc  = fs*2
frame_length = fs*8

files = getFileList(pathData = 'Data/Training_data/', pathTest = 'Data/TestData/')
# whichFile = 5
BPMzero = mean(sapply(1:12, function(i)  readMat(con = files$BPM[[i]])[[1]][1]))

# Full analysis for all availible training files
Error = unlist(mclapply(1:12, function(whichFile)
{ 
  
  # AR model initialization
  BPMteach = unlist(lapply(1:12, function(i)
  {
    if(i != whichFile) BPM = readMat(files$BPM[[i]])[[1]]
  }))
  
  # The second parameter is the weight factor for weighted mean in the result (see function ARmodelTracking())
  ARmodel = list(createARmodel(sig = BPMteach, 10), c(0.7, 1))
  
  BPM = SingleRun(whichFile, plotResult = T, ARmodel)
  
  #Return value is the relative error
  cat('Finished analysis for file', whichFile, sep=' ')
  return(sum(abs(BPM$BPMT-BPM$BPM0)/(BPM$BPM0*length(BPM$BPM0))))
}, mc.cores = cores))


system('echo I have finished the analysis | espeak')

