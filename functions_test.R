library(R.matlab)
library(Rssa)
library(TSA)
library(mgcv)
library(pracma)


ComputeBPM = function(frame, BPMTrajectory, plot = TRUE, fs = 125)
{
  #N_prev = rev(BPMTrajectory)[1]/60*4096/fs
  #N_prev_vect = BPMTrajectory/60*4096/fs
  
  fft_ssr = SubstractionFiltering(frame$ppg, list(X = frame$accx, Y = frame$accy, Z = frame$accz))
  
  # HMM
  BPM_new = HMM(signal = fft_ssr)
  
  BPM = c(BPMTrajectory, BPM_new-offset)
  
  # Plot result if desired
  #if(plot)
#   {
#     if (!is.null(BPM0)) 
#     {
#       plot(BPM0, type='l', ylim=c(0, 200))
#       lines(BPM, col='red')
#     }
#     else
#     {
  
      plot(BPM, type='l',xlim=c(0,160), ylim=c(0, 200))
#     }
#   }
  
  return(BPM)
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

pFFT = function(sig, N = 2*4096)
{
  n = length(sig)
  return(abs(fft(c(sig, rep(0, N-n)))))
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SubstractionFiltering = function(X, A, N=4096*2)
{
  Xfft = pFFT(X, N)
  Yfft = pFFT(A$X, N)+pFFT(A$Y, N)+pFFT(A$Z, N)
  Xscale = sum(Xfft)
  Yscale = sum(Yfft)
  
  Xfft = Xfft/Xscale
  Yfft = Yfft/Yscale
  
  res = Xfft-Yfft
  res[res<0] = 0
  
  return(res*Xscale)
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

convert = function(X, NtoBPM = FALSE, BPMtoN = FALSE, fs = 125)
{
  if(NtoBPM) return (fs*X*60/8192)
  if(BPMtoN) return (X/60*8192/fs)
  else return(NULL)
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

getFileList = function(path = '')
{
  fileDesc = list(data = c(sapply(1:9, function(i) paste('0', i, sep='')), sapply(10, function(i) paste(i))), type = c(rep('01', 4), rep('02', 6)))
  
  RAWfiles = sapply(1:10, function(i) paste('TEST_', fileDesc$data[i], '_T', fileDesc$type[i], '.mat', sep=''))
  #RAWfilesBPM = sapply(1:12, function(i) paste('DATA_', fileDesc$data[i], '_TYPE', fileDesc$type[i], '_BPMtrace.mat', sep=''))
  
  list(data = lapply(1:10, function(i) paste(path, RAWfiles[i], sep='')), BPM = lapply(1:10, function(i) paste(path, sep='')))
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

HMM = function(signal)
{
  states <- convert(50:220,NtoBPM=TRUE)                 
  symbols <- signal[50:220]                             
  emission <- (symbols/sum(symbols))                    
  
{if(i<2){
  BPM_new <- BPM
}
else{
  trans <- dnorm(states,mean=mean(rev(BPM)[1]),sd=20)      
  step[,i] <- trans*emission
  which.max(step[,i])
  BPM_new <- convert(50+which.max(step[,i]),NtoBPM=TRUE)
}}

return(BPM_new)
}
