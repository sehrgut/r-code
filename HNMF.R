
#######################################################################################
## Below is the are the NMF versions that can be used in the project.
#######################################################################################
#
# Parameters:
# A - dictionary matrix
# X - data matrix
# r - parameter of the beta divergence (2 = Euclidean)
# mu - weight of the sparsity regularization
# p - sparsity parameter (1 - Laplace)
# epsilon - ending condition (algorithm stops when the relative objective difference drops below epsilon)
#
NMF <- function(X, A, iterations, r=1, mu=0, p=0.6, epsilon=0.000001, silent=F)
{
  S <- matrix(abs(rnorm(ncol(X)*ncol(A))), ncol=ncol(X))
  objHistory <- c()  # history of the objective

  for(iteration in 1:iterations)
  {
    if(!silent) cat("Iteration ", iteration, " ")
    X_ = A %*% S + 1e-9
    objective <- sum((X - X_)^2)
    objHistory <- c(objHistory, objective) 
    if(!silent) cat("\t objective = ", objective, "\n")
    if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
      break
    S  = S * (t(A) %*% (X / (X_^r))) / (t(A) %*% (X_^(1-r)) + p*(mu + 1e-9)*(S^(p-1)) + 1e-9)
    S[which(S < 0)] = 0  # make sure no weights are negative
   }
  return(list(S = S / max(S), iters = length(objHistory) - 1))
}

NMF.Full <- function(X, A, iterations, r=1, mu=0, p=0.6, epsilon=0.000001, silent=F)
{
  S <- matrix(abs(rnorm(ncol(X)*ncol(A))), ncol=ncol(X))
  objHistory <- c()
  X <- X / max(X)

  for(iteration in 1:iterations)
  {
    if(!silent) cat("Iteration ", iteration, " ")
    X_ = A %*% S + 1e-9
    objective <- sum((X - X_)^2)
    objHistory <- c(objHistory, objective) 
    if(!silent) cat("\t objective = ", objective, "\n")
    if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
      break
    divided <- (X / (X_^r))
    temp2   <- (X_^(1-r))
    A <- A * (divided %*% t(S)) / (temp2 %*% t(S) + 1e-9)
    temp <- t(A) %*% temp2
    if(mu != 0)
      temp <- temp + p*(mu + 1e-9)*(S^(p-1))
    S  <- S * (t(A) %*% divided) / temp
    S[which(S < 0)] = 0
    A <- A / max(A)
    S <- S / max(S)
   }
#   S = S / max(S)
#   A = A / max(S)
  return(list(A=A, S=S))
}

#######################################################################################
## Below are different variants of the NMF. Do not use.
#######################################################################################

# HNMF <- function(X, A, iterations, r=1, mu1=0, mu2=0, mu3=0, p=0.6, epsilon=0.000001, silent=F)
# {
#   S <- matrix(abs(rnorm(ncol(X)*ncol(A))), ncol=ncol(X))
#   W1 <- createWeightingMatrix1(nrow(S))
#   W2 <- createWeightingMatrix2(ncol(S))
#   objHistory <- c()
# 
#   for(iteration in 1:iterations)
#   {
#     if(!silent)
#       cat("Iteration ", iteration, " ")
#     X_ = A %*% S + 1e-9
#     objective <- sum((X - X_)^2)
#     objHistory <- c(objHistory, objective) 
#     if(!silent)
#       cat("\t objective = ", objective, "\n")
#     if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
#       break
#     temp = t(A) %*% (X_^(1-r))
#     if(mu1 != 0)
#       temp = temp + 2*mu1*(W1 %*% S)
#     if(mu2 != 0)
#       temp = temp + p*(mu2 + 1e-9)*(S^(p-1))
#     if(mu3 != 0)
#       temp = temp + exp(-mu3*(S %*% W2))
#     S  = S * (t(A) %*% (X / (X_^r))) / (temp + 1e-9)
# #     S  = S * (t(A) %*% (X / (X_^r))) / (t(A) %*% (X_^(1-r))  + 2*mu1*(W1 %*% S) + p*(mu2 + 1e-9)*(S^(p-1)) + exp(-mu3*(S %*% W2)) + 1e-9)
#     S[which(S < 0)] = 0
#    }
#   return(list(S = S / max(S), iters = length(objHistory) - 1))
# }
# 
# # Calculate both the dictionary A and the weights S
# HNMF.Full <- function(X, A, iterations, r=1, mu1=0, mu2=0, mu3=0, p=0.6, nu1=0, q=2, epsilon=0.000001, silent=F)
# {
#   S <- matrix(abs(rnorm(ncol(X)*ncol(A))), ncol=ncol(X))
#   W1 <- createWeightingMatrix1(nrow(S))
#   W2 <- createWeightingMatrix2(ncol(S))
#   objHistory <- c()
# #   onesMV <- matrix(rep(1, dim(X)[2]), nrow=1)
#   X <- X / max(X)
# 
#   for(iteration in 1:iterations)
#   {
#     if(!silent)
#       cat("Iteration ", iteration, " ")
#     X_ = A %*% S + 1e-9
#     objective <- sum((X - X_)^2)
#     objHistory <- c(objHistory, objective) 
#     if(!silent)
#       cat("\t objective = ", objective, "\n")
#     if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
#       break
#     divided <- (X / (X_^r))
#     temp2   <- (X_^(1-r))
# #     dupa = ((X / (X_^r)) %*% t(S)) / ((X_^(1-r)) %*% t(S) + q*nu1*(A^(q-1)) + 1e-9)
#     A <- A * (divided %*% t(S)) / (temp2 %*% t(S) + 1e-9)
#     temp <- t(A) %*% temp2
#     if(mu1 != 0)
#       temp <- temp + 2*mu1*(W1 %*% S)
#     if(mu2 != 0)
#       temp <- temp + p*(mu2 + 1e-9)*(S^(p-1))
#     if(mu3 != 0)
#       temp <- temp + exp(-mu3*(S %*% W2))
#     S  <- S * (t(A) %*% divided) / temp
#     S[which(S < 0)] = 0
#     A <- A / max(A)
#     S <- S / max(S)
#    }
# #   S = S / max(S)
# #   A = A / max(S)
#   return(list(A=A, S=S))
# }
# 
# HNMF.OnlyS <- function(X, A, iterations, r=1, mu1=0, mu2=0, mu3=0, p=0.6, epsilon=0.000001, silent=F)
# {
#   S <- matrix(abs(rnorm(ncol(X)*ncol(A))), ncol=ncol(X))
#   W1 <- createWeightingMatrix1(nrow(S))
#   W2 <- createWeightingMatrix2(ncol(S))
#   objHistory <- c()
# 
#   for(iteration in 1:iterations)
#   {
#     if(!silent)
#       cat("Iteration ", iteration, " ")
#     X_ = A %*% S + 1e-9
#     objective <- sum((X - X_)^2)
#     objHistory <- c(objHistory, objective) 
#     if(!silent)
#       cat("\t objective = ", objective, "\n")
#     if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
#       break
#     divided <- (X / (X_^r))
#     temp <- t(A) %*% (X_^(1-r))
#     if(mu1 != 0)
#       temp <- temp + 2*mu1*(W1 %*% S)
#     if(mu2 != 0)
#       temp <- temp + p*(mu2 + 1e-9)*(S^(p-1))
#     if(mu3 != 0)
#       temp <- temp + exp(-mu3*(S %*% W2))
#     S  <- S * (t(A) %*% divided) / (temp + 1e-9)
#     S[which(S < 0)] = 0
#    }
#   return(S / max(S))
# }
# 
# HNMF.OnlyA <- function(X, S, A=NULL, iterations, nu1=0, q=2, r=1, epsilon=0.000001, silent=F)
# {
#   if(is.null(A))
#     A <- matrix(abs(rnorm(nrow(X)*nrow(S))), ncol=nrow(S))
#   objHistory <- c()
# 
#   for(iteration in 1:iterations)
#   {
#     if(!silent)
#       cat("Iteration ", iteration, " ")
#     X_ = A %*% S + 1e-9
#     objective <- sum((X - X_)^2)
#     objHistory <- c(objHistory, objective) 
#     if(!silent)
#       cat("\t objective = ", objective, "\n")
#     if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
#       break
#     divided <- (X / (X_^r))
#     temp2   <- (X_^(1-r))
# #     dupa = ((X / (X_^r)) %*% t(S)) / ((X_^(1-r)) %*% t(S) + q*nu1*(A^(q-1)) + 1e-9)
#     A <- A * (divided %*% t(S)) / (temp2 %*% t(S) + 1e-9)
#    }
#   return(A / max(A))
# }
# 
# NMF.IDivergence <- function(X, A, iterations=200, epsilon=0.000001)
# {
#   S <- matrix(abs(rnorm(ncol(X)*ncol(A))), ncol=ncol(X))
#   objective <- sum((X - A%*%S)^2)
#   objHistory <- c(objective)
#   cat("Objective = ", objective, "\n")
#   onesMV <- matrix(rep(1, dim(X)[2]), nrow=1)
#   for(iteration in 1:iterations)
#   {
#     cat("Iteration ", iteration, " ")
#     S <- S * (t(A) %*% (X / (A %*% S + 1e-9))) / (matrix(colSums(A), ncol=1) %*% onesMV)
#     objective <- sum((X - A%*%S)^2)
#     objHistory <- c(objHistory, objective) 
#     cat("\t objective = ", objective, "\n")
#     if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
#       break
#    }
#   return(S / max(S))
# }
# 
# NMF.ItakuraSaito <- function(X, A, iterations=200, epsilon=0.000001)
# {
#   S <- matrix(abs(rnorm(ncol(X)*ncol(A))), ncol=ncol(X))
#   objective <- sum((X - A%*%S)^2)
#   objHistory <- c(objective)
#   cat("Objective = ", objective, "\n")
#   onesMV <- matrix(rep(1, dim(X)[2]), nrow=1)
#   for(iteration in 1:iterations)
#   {
#     cat("Iteration ", iteration, " ")
#     S <- S * (t(A) %*% (X^2 / (A %*% S + 1e-9))) / (matrix(colSums(A), ncol=1) %*% onesMV)
#     objective <- sum((X - A%*%S)^2)
#     objHistory <- c(objHistory, objective) 
#     cat("\t objective = ", objective, "\n")
#     if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
#       break
#    }
#   return(S / max(S))
# }
# 
# NMF.Euclidean <- function(X, A, iterations=200, epsilon=0.000001)
# {
#   S <- matrix(abs(rnorm(ncol(X)*ncol(A))), ncol=ncol(X))
#   objective <- sum((X - A%*%S)^2)
#   objHistory <- c(objective)
#   cat("Objective = ", objective, "\n")
#   for(iteration in 1:iterations)
#   {
#     cat("Iteration ", iteration, " ")
#     S <- S * ((t(A) %*% X) / (t(A) %*% A %*% S))
#     objective <- sum((X - A%*%S)^2)
#     objHistory <- c(objHistory, objective) 
#     cat("\t objective = ", objective, "\n")
#     if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
#       break
#    }
#   return(S / max(S))
# }
# 
# NMF.Raczynski.old <- function(X, A, p=3, iterations=200, epsilon=0.000001)
# {
#   S <- matrix(abs(rnorm(ncol(X)*ncol(A))), ncol=ncol(X))
#   objective <- sum((X - A%*%S)^2)
#   objHistory <- c(objective)
#   cat("Objective = ", objective, "\n")
#   onesMV <- matrix(rep(1, dim(X)[2]), nrow=1)
#   for(iteration in 1:iterations)
#   {
#     cat("Iteration ", iteration, " ")
#     S <- S * (t(A) %*% (X^p / (A %*% S + 1e-9))) / (matrix(colSums(A), ncol=1) %*% onesMV)
#     objective <- sum((X - A%*%S)^2)
#     objHistory <- c(objHistory, objective) 
#     cat("\t objective = ", objective, "\n")
#     if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
#       break
#    }
#   return(S / max(S))
# }
# 
# NMF.Raczynski <- function(X, A, p=3, iterations=200, epsilon=0.000001)
# {
#   S <- matrix(abs(rnorm(ncol(X)*ncol(A))), ncol=ncol(X))
#   objective <- sum((X - A%*%S)^2)
#   cat("Initial objective = ", objective, "\n")
#   for(iteration in 1:iterations)
#   {
#     cat("Iteration ", iteration, " ")
#     X_ = A %*% S + 1e-9
#     S  = S * (t(A) %*% (X / (X_^p))) / (t(A) %*% (X_^(1-p)))
# 
#     newObjective <- sum((X - A%*%S)^2)
#     cat("\t objective = ", newObjective, "\n")
#     if((((objective - newObjective) / objective) < epsilon) && (iteration > 1))
#       break
#    }
#   return(S / max(S))
# }
# 
# NMF.IDivergence.Full <- function(X, basis_size=88, iterations=200, epsilon=0.000001, binsPerOctave=3)
# {
#   A <- matrix(abs(rnorm(nrow(X)*basis_size)), ncol=basis_size)
#   S <- matrix(abs(rnorm(ncol(X)*basis_size)), ncol=ncol(X))
#   objective <- sum((X - A%*%S)^2)
#   objHistory <- c(objective)
#   cat("Objective = ", objective, "\n")
#   onesMV <- matrix(rep(1, dim(X)[2]), nrow=1)
#   onesMH <- matrix(rep(1, dim(X)[1]), ncol=1)
#   for(iteration in 1:iterations)
#   {
#     cat("Iteration ", iteration, " ")
#     A <- A * ((X / (A %*% S + 1e-9)) %*% t(S)) / (onesMH %*% matrix(rowSums(S), nrow=1))
#     S <- S * (t(A) %*% (X / (A %*% S + 1e-9))) / (matrix(colSums(A), ncol=1) %*% onesMV)
#     objective <- sum((X - A%*%S)^2)
#     objHistory <- c(objHistory, objective) 
#     cat("\t objective = ", objective, "\n")
#     if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
#       break
#    }
#   return(list(A=A, S=S))
# }
# 
# NMF.Euclidean.Full <- function(X, basis_size=88, iterations=200, epsilon=0.000001, binsPerOctave=3)
# {
#   A <- matrix(abs(rnorm(nrow(X)*basis_size)), ncol=basis_size)
#   S <- matrix(abs(rnorm(ncol(X)*basis_size)), ncol=ncol(X))
#   objective <- sum((X - A%*%S)^2)
#   objHistory <- c(objective)
#   cat("Objective = ", objective, "\n")
#   for(iteration in 1:iterations)
#   {
#     cat("Iteration ", iteration, " ")
#     A <- A * (X %*% t(S)) / (A %*% S %*% t(S))
#     S <- S * ((t(A) %*% X) / (t(A) %*% A %*% S))
#     objective <- sum((X - A%*%S)^2)
#     objHistory <- c(objHistory, objective) 
#     cat("\t objective = ", objective, "\n")
#     if(((abs(objHistory[length(objHistory)-1] - objective) / objHistory[length(objHistory)-1]) < epsilon) && (iteration > 1))
#       break
#    }
#   return(list(A=A, S=S))
# }
